import React from "react";
import "./Header.css";
import NavBar from "./Navbar";
import LogoHeader from "./logo";

function Header() {
  return (
    <header className="Entête">
      <LogoHeader />
      <NavBar />
    </header>
  );
}

export default Header;
