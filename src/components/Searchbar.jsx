import React, { useState, useEffect } from "react";
import "./Searchbar.css";
import axios from "axios";

function Searchbar() {
  const [search, setSearch] = useState("");
  // useEffect(() => {
  //   axios.get("https://api.themoviedb.org/2/movie/popular").then((response) => {
  //     response.data.results;
  //     setSearch(response.data.results);
  //   });
  // }, []);

  //if (search) {
  return (
    <div className="Searchbar">
      <input className="Search" type="text" placeholder="Rechercher..." />
      <button type="submit" className="Loupe">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="1.75rem"
          height="1.75rem"
          viewBox="0 0 40 40"
          fill="none"
        >
          <path
            d="M39.456 34.5826L31.6664 26.7943C31.3148 26.4427 30.8382 26.2474 30.3382 26.2474H29.0646C31.221 23.4899 32.5024 20.0215 32.5024 16.2484C32.5024 7.27273 25.2284 0 16.2512 0C7.27397 0 0 7.27273 0 16.2484C0 25.2241 7.27397 32.4968 16.2512 32.4968C20.0249 32.4968 23.4939 31.2157 26.2519 29.0597V30.333C26.2519 30.8329 26.4472 31.3094 26.7988 31.661L34.5885 39.4493C35.3229 40.1836 36.5105 40.1836 37.2371 39.4493L39.4482 37.2386C40.1826 36.5042 40.1826 35.3169 39.456 34.5826ZM16.2512 26.2474C10.7273 26.2474 6.25046 21.7791 6.25046 16.2484C6.25046 10.7255 10.7195 6.24939 16.2512 6.24939C21.775 6.24939 26.2519 10.7177 26.2519 16.2484C26.2519 21.7713 21.7828 26.2474 16.2512 26.2474Z"
            fill="white"
          />
        </svg>
      </button>
    </div>
  );
  //}
}

export default Searchbar;
