

function CardsItem(props) {


    return (
        <>
            <div className="cards--movies" >
                <h2>{props.titre}</h2>
                <h3> {props.date}</h3>
                <img className="cards--img" src={"https://www.themoviedb.org/t/p/w1280/" + props.image}></img>


            </div>
        </>
    )
}

export default CardsItem;