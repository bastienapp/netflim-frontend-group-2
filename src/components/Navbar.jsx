import "./Navbar.css";

function NavBar() {
  return (
    <nav className="navbar">
      <ul>
        <li>
          <a href="/">Accueil</a>
        </li>
        <li>
          <a href="/categories">Catégories</a>
        </li>
        <li>
          <a href="/profile">Profil</a>
        </li>
      </ul>
    </nav>
  );
}

export default NavBar;
