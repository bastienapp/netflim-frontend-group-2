import React from "react";
import "./Logo.css";
import logoImage from "../assets/logo-netflim.png";

const LogoHeader = () => {
  return (
      <img src={logoImage} alt="Logo" className="logo" />
  );
};

export default LogoHeader;
