function Title(props) {
  const { content } = props;
  return <h1>{content}</h1>;
}

export default Title;
