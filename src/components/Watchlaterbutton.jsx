import React, { useState, useEffect } from 'react';
import eyeImage from '../assets/eye.png';
import closedEye from '../assets/closedeye.png'

import '../components/WatchLaterButton.css';

function WatchLaterButton(props) {
    const [addedToAvoir, setAddedToAvoir] = useState(false);
    useEffect(() => {
        const storedValue = localStorage.getItem('addedToAvoir');
        if (storedValue !== null) {
            setAddedToAvoir(JSON.parse(storedValue));
        }
    }, []);

   
    const toggleAvoir = () => {
        const newValue = !addedToAvoir;
        setAddedToAvoir(newValue);
        localStorage.setItem('addedToAvoir', JSON.stringify(newValue));
    };

    return (
        <div>
            <button className="a-voir" onClick={toggleAvoir}>
            <img src={addedToAvoir ? eyeImage : closedEye} alt="Watch Later Button" className='imageavoir'/>
            </button>
        </div>
    );
}

export default WatchLaterButton;
