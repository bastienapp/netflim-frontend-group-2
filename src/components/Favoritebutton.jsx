import React, { useState, useEffect } from "react";
import heartImage from "../assets/heart.png";
import emptyHeart from "../assets/emptyheart.png";

import "../components/FavoriteButton.css";

function FavoriteButton(props) {
  const [addedToMesFavoris, setAddedToMesFavoris] = useState(false);

  // Récupérer la valeur du localStorage lors de la création du composant
  useEffect(() => {
    const storedValue = localStorage.getItem("addedToMesFavoris");
    if (storedValue !== null) {
      setAddedToMesFavoris(JSON.parse(storedValue));
    }
  }, []);

  // Fonction pour changer l'état et enregistrer dans le localStorage
  const toggleMesFavoris = () => {
    const newValue = !addedToMesFavoris;
    setAddedToMesFavoris(newValue);
    localStorage.setItem("addedToMesFavoris", JSON.stringify(newValue));
  };

  return (
    <div>
      <button className="boutonfavoris" onClick={toggleMesFavoris}>
        <img
          src={addedToMesFavoris ? heartImage : emptyHeart}
          alt="Watch Later Button"
          className="imageavoir"
        />
      </button>
    </div>
  );
}

export default FavoriteButton;
