import { NavLink } from "react-router-dom"

function ProfileCard({ title, icon, chemin }) {
  return (
    <NavLink to={chemin}>
    <div className="profile-card">
    <div className='profile-card-title'>
      <h3>{title}</h3>
    </div>
    <div className='profile-card-body'>
      <img src={icon} alt="icône des cartes"/>
    </div>
    </div>
    </NavLink>
  )
  }
  
export default ProfileCard