import ProfileCard from "./ProfileCard"
import ProfileUser from "./ProfileUser"
import "./ProfilePage.css"
import ProfileUserQuoteButton from "./ProfileUserQuoteButton"

function ProfilePage() {
  const user = {
    personName: "Michel Tacos",
    avatar: "https://i.stack.imgur.com/FxJvk.jpg",
    quote: <ProfileUserQuoteButton />
  }
  return (
    <>
    <div className="profile-page">
      <div className="profile-banner">
    < ProfileUser {...user} />
    </div>
    <br>
    </br>
    <br>
    </br>
    <br>
    </br>
    <br>
    </br>
    <div className="profile-cards-block"> 
 
    < ProfileCard title="Mes notes" icon="src/assets/Icons/Star.svg" chemin='/'/>
    < ProfileCard title="Mes favoris" icon="src/assets/Icons/Heart.svg" chemin='/' />
    < ProfileCard title="Mes films à voir" icon="src/assets/Icons/Eye.png" chemin='/' />
  </div>
  </div>
  </>
  )
}

export default ProfilePage