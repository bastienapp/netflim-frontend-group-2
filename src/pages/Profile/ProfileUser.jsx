import ProfileUserLegendButton from "./ProfileUserQuoteButton";

function ProfileUser({ personName, avatar, quote}) {
  return (
    <div className="profile-user">
    <img src={avatar} alt= "User Avatar" />
    <h2 className="user-name"> {personName}</h2>
    <p className="user-quote">{quote}</p>
    </div>
  );
};

export default ProfileUser