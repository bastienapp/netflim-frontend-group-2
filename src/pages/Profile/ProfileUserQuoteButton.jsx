import axios from "axios"
import { useState, useEffect } from "react";

const movieQuoteUrl = "https://api.quotable.io/random"

function ProfileUserQuoteButton() {

  const [randomQuote, setRandomQuote] = useState(null);

  const handleEditClick = () => {
    loadAPI();
  };
  useEffect(() => {
    loadAPI();
  }, []);

  function loadAPI() {
    axios.get(movieQuoteUrl)
      .then((response) => {
        setRandomQuote(response.data);
        console.log(response.data)
      })
      .catch((error) => {
        console.error("Erreur de récupération de citation:", error)
      });
  }

  if (!randomQuote) return null;

  return (
    <div className="button">
      <p className="quote-content">{randomQuote.content}</p>
      <p>{randomQuote.author}</p>
      <button type="button" className="quote-button" onClick={handleEditClick}> Nouvelle citation </button>
      
 </div>
  )
}

export default ProfileUserQuoteButton