import Searchbar from "../../components/Searchbar";
import Title from "../../components/Title";
import FavoriteButton from "../../components/Favoritebutton";
import WatchLaterButton from "../../components/Watchlaterbutton";
import Cards from "../../components/Cards";

function HomePage() {
  
  return (
    <>
      <Title content="Netflim" />
      <Searchbar />
      <p>
        Bienvenue sur le merveilleux site Netflim des Black Eyed Peas in a Pod
      </p>
      <FavoriteButton />
      <WatchLaterButton/>
      <Cards />

    </>
  );
}

export default HomePage;
