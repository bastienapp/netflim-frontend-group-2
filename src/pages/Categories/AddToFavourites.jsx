import { useState } from "react";

function AddToFavourites(props) {
    const { title, released, director, poster } = props;

    const [favourite, setFavourite] = useState(false);
    const toggleFavourite = () => {
        setFavourite(!favourite);
    }
    return (
        <>
            <img src={poster} alt={title} />
            <h4>Director: {director}</h4>
            <h5>Released: {released}</h5>
            <button type="button">Ajouter aux favoris</button>
            <button type="button" onClick={toggleFavourite}> {favourite ? "Retirer des favoris" : "Ajouter aux favoris"} </button>
        </>
    );
}
export default AddToFavourites