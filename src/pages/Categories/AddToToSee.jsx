import { useState } from "react";

function AddToToSee() {
    const [toSee, setToSee] = useState(false);
    const toggleToSee = () => {
        setToSee(!toSee);
    }

    return (
        <div className="MovieToSee">
            <button type="button">Ajouter aux films à voir</button>
            <button type="button" onClick={toggleToSee}> {toSee ? "Retirer des films à voir" : "Ajouter aux films à voir"} </button>
        </div>
    )
}

export default AddToToSee