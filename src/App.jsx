import React from "react";
import HomePage from "./pages/Home/HomePage";
import Header from "./components/Header";
import Searchbar from "./components/Searchbar";
import ProfilePage from "./pages/Profile/ProfilePage";

function App() {
 
     
  return (
    <>
      <Header />
      <Searchbar />
      <HomePage />
      <ProfilePage />
    </>
  );
}
export default App;
